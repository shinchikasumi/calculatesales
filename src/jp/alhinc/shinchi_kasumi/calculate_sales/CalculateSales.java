package jp.alhinc.shinchi_kasumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;



public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;
		try {
			File branchFile = new File(args[0], "branch.lst");

			//1.支店定義ファイルがないときのエラー
			if(!branchFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			br = new BufferedReader(new FileReader(branchFile));

			//★mapやファイルの名前を一目見て分かるように修正　→修正済み
			HashMap<String,String> branchMap = new HashMap<>();
			HashMap<String,Long> totalMap = new HashMap<>();

			String brLine;
			while((brLine = br.readLine()) != null) {
				if(!brLine.contains(",")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//lineデータを支店コードと支店名に分割
				String[] data = brLine.split(",");

			    //2.支店定義ファイルのフォーマット不正のエラー
			    //支店コード：数字のみ3桁
				//支店名：カンマ、改行を含まない文字列
				int x = data.length;
				if(!data[0].matches("[0-9]{3}") || x != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
            		return;
            	}

			    //1つ目の文字列をリストに追加
				String id = data[0];
				String branchName = data[1];

	            //支店コード、支店名 中身が分かるように名前をつける
				branchMap.put(id, branchName);
			    //支店コード、0
				totalMap.put(id, 0L);
			}

		    //拡張子と数字8桁でフィルタする
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {
					return str.matches("[0-9]{8}.rcd");
	           	}
			};

			File[] rcdFile  = new File(args[0]).listFiles(filter);

            //データの読み込み
			for(int i = 0; i < rcdFile.length; i++) {

            	//3.rcdファイルのNo.が連番になっていない場合のエラー
				//ファイル名の取得
				String fileName = rcdFile[i].getName();

              	//拡張子なしのファイル名
            	String name = fileName.substring(0, fileName.lastIndexOf('.'));
            	//int型に変換
            	int nameNumber = Integer.parseInt(name);
            	if(nameNumber != 1 + i) {
            		System.out.println("売上ファイル名が連番になっていません");
            		return;
            	}

				BufferedReader br1 = new BufferedReader(new FileReader(rcdFile[i]));

            	//配列をつくる
            	ArrayList<String> salesList = new ArrayList<>();

            	//リストにデータを追加 001,209800...
            	String slLine;
            	while((slLine = br1.readLine()) != null) {
            		salesList.add(slLine);
            	}
            	br1.close();

        		//6.売上ファイルの中身が3行以上ある場合のエラー★一行でも駄目　→修正済み
            	if(salesList.size() != 2) {
		   			System.out.println(fileName  + "のフォーマットが不正です");
            		return;
		   		}

            	//sales = 売上金額★変数は名詞
            	String sales = salesList.get(1);
            	Long longSales = Long.parseLong(sales);

        		String code = salesList.get(0);


	     		//5.支店に該当がない場合のエラー
	     		if(!branchMap.containsKey(code)) {
	     			System.out.println(fileName  +  "の支店コードが不正です");
	     			return;
	     		}

	     		Long totalSales = totalMap.get(code) + longSales; // 0+金額
	     		totalMap.put(code, totalSales);

		   		//桁数の確認 ★やり方が間違っている →修正済み
	     		int num = totalSales.toString().length();

		    	//4.合計金額が10桁を超えた場合のエラー
		   		if(num > 10) {
	    			System.out.println("合計金額が10桁を超えました");
            		return;
	    		}
		   	}
			File writeFile = new File(args[0], "branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(writeFile));

			for (HashMap.Entry<String, Long> entry : totalMap.entrySet()) {
		   		bw.write(entry.getKey() + "," + branchMap.get(entry.getKey()) + "," + entry.getValue());
		   		bw.newLine();
		   	}
			bw.close();

		} catch(IOException e) {
		   	System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
};
